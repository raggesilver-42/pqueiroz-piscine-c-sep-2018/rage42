
# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White


function abspath {
	local _path=$1
	local cur=`pwd`

	cd $_path
	echo `pwd`
	cd $cur
}

# Takes 2 params, first is a color, second is a string
# Does not print a new line
function print {
	printf "${1}${2}${Color_Off}"
}

# Chekc if the repo has a .gitignore
function check_gitignore {
	local ig="${cwd}/.gitignore"
	if [ ! -f "$ig" ]; then

		# If there is no .gitignore add the template
		echo ".gitignore\n*test*\n*.o\n*.*~\n.rage*\n._*\ncheck.sh\n*backup*"\
			> $ig
		
		exit 0

	fi
}

function add_if_not_ignored {
	local ff=$1
	git check-ignore $ff
	local oc=$?
	if [[ $oc -eq 1 ]]; then
		git add $ff
		echo Add `basename "$ff"`
	else
		print $Yellow "Skipping `basename "$ff"`"
		echo ", gitignored"
	fi
}

function mk {
	local $ff=$1
	
	mkdir -p .rage
	rm -rf .rage/*

	###
}

function check_git_repo {
	local cur=`pwd`
	cd $cwd
	if [[ ! `git rev-parse --is-inside-work-tree 2> /dev/null` ]]; then
		echo "'$cwd' is not a git repo"
		exit 1
	fi

	cd $cur
	check_gitignore
}

function check_for_updates {
	local script_location=`abspath $(dirname $0)`
	local _cur=`pwd`

	cd $script_location

	if [[ `git rev-parse --is-inside-work-tree 2> /dev/null` ]]; then

		if [[ `git status -sb` == "*behind*" ]]; then
			echo "There are updates for this script."
			# Ask if the student wants to commit
			# read -r -p "Do you want to update? [n/Y]" -i y response
			# case "$response" in
			# 	[yY][eE][sS]|[yY])
			# 		# Get the commit message
			# 		echo "Do update"
			# 		exit 0
			# 		;;
			# 	*) cd $_cur; return;;
			# esac
			#
		else
			echo "No updates"
		fi
	else
		echo "Not a git repo"
		echo `pwd`
	fi

	cd $_cur;
}

#
# Globals
d=
enable_git=false
cwd=`pwd`

check_for_updates

#
# Get arguments and flags
while true ; do
	case "$1" in
		--git )
			enable_git=true; shift;;
		-- )
			shift; break ;;
		* )
			break;
	esac
done

#
# If the student specified another argument
if [[ $# -eq 1 ]]; then
	# If the argument is not a directory
	if [[ ! -d $1 ]]; then
		echo "Invalid argument \"$1\""
		exit 1
	fi
	# If it is get it's abspath and store it in cwd
	cwd=$(abspath $1)
elif [[ $# -gt 1 ]]; then
	echo "Too many arguments"
	echo $1
	exit 1
fi

d=`basename $cwd`

#
# If git is enabled, do the required checks
if $enable_git; then
	check_git_repo
fi

# folders=`find "$cwd" -name "ex*" -type d -maxdepth 1`
source "./${d}.sh"

folders=$RAGE_EXERCISES

#
# The actual program
for fold in "${RAGE_EXERCISES[@]}"; do
	
	fold="$cwd/$fold"

	print $White "==========================================================\n"

	if [ ! -d "$fold" ]; then
		print $Cyan "Folder $fold does not exist"
		echo ", skipping..."
		continue
	fi

	cd $fold

	files=`find "${fold}" -name "*.c" -type f -maxdepth 1 2> /dev/null`

	if [[ ${#files} -le 1 ]]; then
		print $Cyan "No source files"
		echo " in $fold, skipping..."
		cd ..
		continue
	fi
	
	out=`norminette -R CheckForbiddenSourceHeader *.c 2> /dev/null`

	if [[ "$out" == "*Error*" ]]; then
		echo "Norminette return an error for $fold"
		echo $out
		exit 1
	fi

	printf "%s" "Norm on `basename $fold` "
	print $Green "OK\n"

	rage_do_correction

	if $enable_git; then
		for f in $files; do
			add_if_not_ignored $f
		done
	fi

	cd ..

done

#
# Post git operations
if $enable_git; then
	git status

	# If there are staged files waiting to be commited
	cmd=`git diff --cached --exit-code 2> /dev/null`
	ext=$?
	if [[ $ext -ne 0 ]]; then

		# Ask if the student wants to commit
		read -r -p "Do you want to commit? [y/N]" response
		case "$response" in
			[yY][eE][sS]|[yY])
				# Get the commit message
				read -r -p "Commit message: " msg
				git commit -m "$msg"
				;;
			*) break;;
		esac

	fi

fi