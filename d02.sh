#
#
# This loads the exercises for d03

function rage_do_correction {
    RAGE_EXPECTED_FILE=
    RAGE_EXPECTED_FUNCTION=
    RAGE_EXPECTED_OUTPUT=""
    RAGE_RUN=""
    RAGE_EXPECTED_RETURN_CODE=0
    ft_putchar=false

    case $RAGE_CORRECTION_INDEX in
        0|"0" )
            RAGE_EXPECTED_FUNCTION="ft_print_alphabet"
            RAGE_EXPECTED_FILE="ft_print_alphabet.c"
            RAGE_RUN="https://gitlab.com/snippets/1756050/raw"
            RAGE_EXPECTED_OUTPUT="abcdefghijklmnopqrstuvwxyz"
            ft_putchar=true
            ;;
        1|"1" )
            RAGE_EXPECTED_FUNCTION="ft_print_reverse_alphabet"
            RAGE_EXPECTED_FILE="ft_print_reverse_alphabet.c"
            RAGE_RUN="https://gitlab.com/snippets/1756058/raw"
            RAGE_EXPECTED_OUTPUT="zyxwvutsrqponmlkjihgfedcba"
            ft_putchar=true
            ;;
        2|"2" )
            RAGE_EXPECTED_FUNCTION="ft_print_numbers"
            RAGE_EXPECTED_FILE="ft_print_numbers.c"
            RAGE_RUN="https://gitlab.com/snippets/1756059/raw"
            RAGE_EXPECTED_OUTPUT="0123456789"
            ft_putchar=true
            ;;
        3|"3" )
            RAGE_EXPECTED_FUNCTION="ft_is_negative"
            RAGE_EXPECTED_FILE="ft_is_negative.c"
            RAGE_RUN="https://gitlab.com/snippets/1756061/raw"
            RAGE_EXPECTED_OUTPUT="P"
            ft_putchar=true
            ;;
        4|"4" )
            RAGE_EXPECTED_FUNCTION="ft_print_comb"
            RAGE_EXPECTED_FILE="ft_print_comb.c"
            RAGE_RUN="https://gitlab.com/snippets/1756063/raw"
            RAGE_EXPECTED_OUTPUT="012, 013, 014, 015, 016, 017, 018, 019, 023, 024, 025, 026, 027, 028, 029, 034, 035, 036, 037, 038, 039, 045, 046, 047, 048, 049, 056, 057, 058, 059, 067, 068, 069, 078, 079, 089, 123, 124, 125, 126, 127, 128, 129, 134, 135, 136, 137, 138, 139, 145, 146, 147, 148, 149, 156, 157, 158, 159, 167, 168, 169, 178, 179, 189, 234, 235, 236, 237, 238, 239, 245, 246, 247, 248, 249, 256, 257, 258, 259, 267, 268, 269, 278, 279, 289, 345, 346, 347, 348, 349, 356, 357, 358, 359, 367, 368, 369, 378, 379, 389, 456, 457, 458, 459, 467, 468, 469, 478, 479, 489, 567, 568, 569, 578, 579, 589, 678, 679, 689, 789"
            ft_putchar=true
            ;;
        5|"5" )
            RAGE_EXPECTED_FUNCTION="ft_print_comb2"
            RAGE_EXPECTED_FILE="ft_print_comb2.c"
            RAGE_RUN="https://gitlab.com/snippets/1756066/raw"
            curl -o /tmp/rage/g https://gitlab.com/snippets/1756065/raw
            RAGE_EXPECTED_OUTPUT="`cat /tmp/rage/g`"
            ft_putchar=true
            ;;
        6|"6" )
            RAGE_EXPECTED_FUNCTION="ft_putnbr"
            RAGE_EXPECTED_FILE="ft_putnbr.c"
            RAGE_RUN="https://gitlab.com/snippets/1756067/raw"
            RAGE_EXPECTED_OUTPUT="42"
            ft_putchar=true
            ;;
        * )
            print $White "No auto-check for ${RAGE_CORRECTION_INDEX}, skipping...\n"
            RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
            return
            ;;
    esac

    print $Cyan "Correcting ex ${RAGE_CORRECTION_INDEX}\n"

    #
    # If the required file does not exist
    if [[ ! -f "$RAGE_EXPECTED_FILE" ]]; then
        echo "Required file '$RAGE_EXPECTED_FILE' doesn't exist"
        echo `pwd`
        exit 1
    fi

    local rage_current=`pwd`

    #
    # Create the tmp folder, copy the files, and cd into it
    mkdir -p "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    cp -r ./* "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}/"
    cd "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"

    local inc_files="$RAGE_EXPECTED_FILE"

    if [[ $RAGE_RUN == "" ]]; then
        cd $rage_current
        print $White "No auto-tests for ex ${RAGE_CORRECTION_INDEX}, skipping...\n"
        RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
        return
    fi

    if $ft_putchar; then
        inc_files="${inc_files} ft_putchar.c"
        curl -s -o ft_putchar.c https://gitlab.com/snippets/1756020/raw
    fi

    inc_files="${inc_files} main_print.c"
    curl -s -o main_print.c https://gitlab.com/snippets/1756021/raw

    curl -s -o run.h $RAGE_RUN

    #
    # Compile the program
    print $Cyan "Compiling"
    echo " gcc -o a.out $inc_files $compile_flags"
    gcc -o "a.out" $inc_files $compile_flags
    local out=$?

    #
    # If compiling failed
    if [[ $out -ne 0 ]]; then
        print $Red "Compiling exited with non-zero code ($out)\n"
        exit 1
    fi

    local _output=`/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}/a.out`
    out=$?

    #
    # If running failed
    if [[ $out -ne $RAGE_EXPECTED_RETURN_CODE ]]; then
        print $Red "Running exited with non-expected code ($out)\n"
        exit 1
    fi

    echo "Output =="
    echo $_output | cat -e
    echo "Expected =="
    echo $RAGE_EXPECTED_OUTPUT | cat -e

    #
    # If output is not the expedted
    if [[ "$_output" != "$RAGE_EXPECTED_OUTPUT" ]]; then
        print $Red "Running outputed an unexpected result\n"
        exit 1
    fi

    # ls "/tmp/rage/${d}/ex${RAGE_CORRECTION_INDEX}"
    print $Green "Compile and run successful, ex ${RAGE_CORRECTION_INDEX} OK!\n"

    cd $rage_current

    RAGE_CORRECTION_INDEX=$(( RAGE_CORRECTION_INDEX + 1 ))
}

RAGE_FIRST=false
RAGE_LAST=
RAGE_CORRECTION_INDEX=
RAGE_EXERCISES=()
compile_flags="-Wall -Wextra -Werror $FLAGS"

for i in {0..7}; do

    if [[ $RAGE_FIRST == false ]]; then
        RAGE_FIRST="$i"
    fi

    RAGE_LAST="$i"

    name="ex${i}"
    if [[ $i -le 9 ]]; then name="ex0${i}"; fi
    RAGE_EXERCISES[$i]="$name"

done

RAGE_CORRECTION_INDEX="$RAGE_FIRST"

mkdir -p "/tmp/rage/${d}"
rm -rf "/tmp/rage/${d}"
